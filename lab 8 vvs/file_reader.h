#ifndef FILE_READER_H
#define FILE_READER_H

#include "banki.h"
#include <iostream>
using namespace std;

void read(const char* file_name, information* array[], int& size);

#endif
