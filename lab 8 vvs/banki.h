#ifndef BANKI_H
#define BANKI_H

#include "constants.h"
#include <string>
#include <iostream>
using namespace  std;

struct Bank
{
	char name[MAX_STRING_SIZE];

	char ylica[MAX_STRING_SIZE];
};

struct Kurs
{
	double pokupka;

	double prodaja;
};

struct information
{
	Bank nazvanie;
	Kurs tekuschii;
	char title[MAX_STRING_SIZE];
};

#endif