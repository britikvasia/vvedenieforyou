#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>
#include <iostream>
#include <string>
using namespace std;

void read(const char* file_name, information* array[], int& size)
{
    ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            information* item = new information;
            file >> item->nazvanie.name;
            file >> tmp_buffer;
            file >> item->tekuschii.pokupka;
            file >> tmp_buffer;
            file >> item->tekuschii.prodaja;
            file >> tmp_buffer;
            file >> item->nazvanie.ylica;
            file.getline(item->title, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        {
            throw "������ �������� �����";
        }
    }
}
    